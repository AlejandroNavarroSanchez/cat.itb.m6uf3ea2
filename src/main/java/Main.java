import ClassesJava.pojo.Person;
import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.push;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class Main {
    public static void main(String[] args) {

        // EXERCISES
        // 1
//        uploadDocs("ITB", "products2", "src/main/resources/products.json");
//        uploadDocs("ITB", "students2", "src/main/resources/students.json");
        // 2
//        uploadArrayDocs("ITB", "people2", "src/main/resources/people.json");
//        uploadPojoDocs("ITB", "people2", "src/main/resources/people.json");
        // 3
//        updateProductStock("ITB", "products2",600, 1000, 150);
//        addDiscount("ITB", "products2", "discount", 0.20);
//        addCategory("ITB", "products2", "Apple TV", "smartTV");
        // 4
//        selectContactById("ITB", "students2", "07517306D");
//        prom84Students("ITB", "students2", 1984);
//        friendsOf("ITB", "people2", "Jessica Adamson");
        // 5
//        deleteProducts("ITB", "products2",400, 600);
//        deleteStudent("ITB", "students2","Daniel", "Vintro");
        // 6
//        countAllDocs("ITB", "students2");
//        collectionExists("ITB", "students2");

        ConnectionMongoCluster.closeConnection();
    }

    /**
     * Method to check if a collection exists or not.
     * @param database String
     * @param collection String
     */
    private static void collectionExists(String database, String collection) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);

        MongoIterable<String> col =  db.listCollectionNames();
        for(String s : col) {
            if(s.equals(collection)) {
                System.out.println("La collection existeix.");;
            }
        }
        System.out.println("La col·lecció '" + collection + "' no existeix.");
    }

    /**
     * Method to drop a collection from the database
     * @param database String
     * @param collection String
     */
    private static void dropCollection(String database, String collection) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Drop collection
        System.out.println("Esborrant " + collection);
        db.getCollection(collection).drop();
    }

    /**
     * Method to count all documents in a collection.
     * @param database String
     * @param collection String
     */
    private static void countAllDocs(String database, String collection) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        System.out.println("Total de documents a " + collection + ": " + col.countDocuments());

        // Drop collection
        dropCollection(database, collection);

    }

    /**
     * Method to remove a student from the collection
     * @param database String
     * @param collection String
     * @param name String
     * @param lastNam String
     */
    private static void deleteStudent(String database, String collection, String name, String lastNam) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        // Delete Many
        DeleteResult deleteResultCompany = col.deleteOne(and(eq("firstname", name), eq("lastname1", lastNam)));
        System.out.println("Registres esborrats: "+ deleteResultCompany.getDeletedCount()+"\n");
    }

    /**
     * Method to delete certain products from a collection in a price range
     * @param database String
     * @param collection String
     * @param min int
     * @param max int
     */
    private static void deleteProducts(String database, String collection, int min, int max) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        // Delete Many
        DeleteResult deleteResultCompany = col.deleteMany(and(gt("price", min), lt("price", max)));
        System.out.println("Registres esborrats: "+ deleteResultCompany.getDeletedCount()+"\n");
    }

    /**
     * Method to find all friends of a specific student
     * @param database String
     * @param collection String
     * @param person String
     */
    private static void friendsOf(String database, String collection, String person) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        FindIterable<Document> listStudents = col.find(eq("name", person))
                .projection(new Document("_id", 0)
                        .append("friends", 1)
                );
        listStudents.forEach( d -> {
            System.out.println(d.toJson());
        });
    }

    /**
     * Method to find all students that where born in 1984
     * @param database String
     * @param collection String
     * @param year int
     */
    private static void prom84Students(String database, String collection, int year) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        FindIterable<Document> listStudents = col.find(eq("birth_year", year));
        listStudents.forEach( s -> {
            System.out.println(s.toJson());
        });
    }

    /**
     * Method to show contact information of a student by his/her PID
     * @param database String
     * @param collection String
     * @param dni String
     */
    private static void selectContactById(String database, String collection, String dni) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        FindIterable<Document> listStudents = col.find(eq("dni", dni))
                .projection(new Document("_id", 0)
                        .append("email", 1)
                        .append("phone", 1)
                        .append("phone_aux", 1)
        );
        listStudents.forEach( s -> {
            System.out.println(s.toJson());
        });
    }

    /**
     * Method to add a category to a certain product
     * @param database String
     * @param collection String
     * @param name String
     * @param category String
     */
    private static void addCategory(String database, String collection, String name, String category) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        UpdateResult updateResult = col.updateMany(gt("stock", 100), push("categories", category));
        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");
    }

    /**
     * Method to add a column and it's value when the stock is higher than 100
     * @param database String
     * @param collection String
     * @param newColumn String
     * @param discount double
     */
    private static void addDiscount(String database, String collection, String newColumn, double discount) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        UpdateResult updateResult = col.updateMany(gt("stock", 100), set(newColumn, discount));
        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");
    }

    /**
     * Method to update a product's stock between a price range
     * @param database String
     * @param collection String
     * @param min int
     * @param max int
     * @param stock int
     */
    private static void updateProductStock(String database, String collection, int min, int max, int stock) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        UpdateResult updateResult = col.updateMany(and(gte("price", min), lte("price", max)), set("stock", stock));
        System.out.println("Registres actualitzats: "+ updateResult.getModifiedCount()+"\n");
    }

    /**
     * Method to upload POJOS as documents to a collection into the database
     * @param database String
     * @param collection String
     * @param file String
     */
    private static void uploadPojoDocs(String database, String collection, String file) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);
        db = db.withCodecRegistry(pojoCodecRegistry);

        // Drop collection before populating
        db.getCollection(collection).drop();

        // Collection
        MongoCollection<Person> col = db.getCollection(collection, Person.class);

        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);

        for (Person person : people) {
            String personJSON = gson.toJson(person);
            System.out.println(personJSON);
            col.insertOne(person);
        }

        ConnectionMongoCluster.closeConnection();
    }

    /**
     * Method to upload and Array as documents to a collection into the database
     * @param database String
     * @param collection String
     * @param file String
     */
    private static void uploadArrayDocs(String database, String collection, String file) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);

        // Drop collection before populating
        db.getCollection(collection).drop();

        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile+=linea;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        Person[] people = gson.fromJson(stringFile, Person[].class);

        for (Person person : people) {
            String personJSON = gson.toJson(person);
            System.out.println(personJSON);
            col.insertOne(Document.parse(personJSON));
        }

        ConnectionMongoCluster.closeConnection();
    }

    /**
     * Method to upload all lines of a file as documents to a collection into the database
     * @param database String
     * @param collection String
     * @param file String
     */
    public static void uploadDocs(String database, String collection, String file) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase(database);

        // Drop collection before populating
        db.getCollection(collection).drop();

        // Collection
        MongoCollection<Document> col = db.getCollection(collection);

        List<Document> collectionList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                collectionList.add(Document.parse(linea));
                System.out.println("Row: " + linea);
            }
            System.out.println();

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        col.insertMany(collectionList);
    }
}
